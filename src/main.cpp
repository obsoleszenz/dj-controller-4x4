#include "main.h"

Multiplexer* multiplexer = new Multiplexer();

BaseComponent* components[] = {
  new MidiPotentiometer(A0, MIDI_CTRL_ROW_ONE,   MIDI_CHANNEL_ONE),
  new MidiPotentiometer(A0, MIDI_CTRL_ROW_TWO,   MIDI_CHANNEL_ONE),
  new MidiPotentiometer(A0, MIDI_CTRL_ROW_THREE, MIDI_CHANNEL_ONE),
  new MidiPotentiometer(A0, MIDI_CTRL_ROW_FOUR,  MIDI_CHANNEL_ONE),
  new MidiPotentiometer(A0, MIDI_CTRL_ROW_ONE,   MIDI_CHANNEL_TWO),
  new MidiPotentiometer(A0, MIDI_CTRL_ROW_TWO,   MIDI_CHANNEL_TWO),
  new MidiPotentiometer(A0, MIDI_CTRL_ROW_THREE, MIDI_CHANNEL_TWO),
  new MidiPotentiometer(A0, MIDI_CTRL_ROW_FOUR,  MIDI_CHANNEL_TWO),
  new MidiPotentiometer(A0, MIDI_CTRL_ROW_ONE,   MIDI_CHANNEL_THREE),
  new MidiPotentiometer(A0, MIDI_CTRL_ROW_TWO,   MIDI_CHANNEL_THREE),
  new MidiPotentiometer(A0, MIDI_CTRL_ROW_THREE, MIDI_CHANNEL_THREE),
  new MidiPotentiometer(A0, MIDI_CTRL_ROW_FOUR,  MIDI_CHANNEL_THREE),
  new MidiPotentiometer(A0, MIDI_CTRL_ROW_ONE,   MIDI_CHANNEL_FOUR),
  new MidiPotentiometer(A0, MIDI_CTRL_ROW_TWO,   MIDI_CHANNEL_FOUR),
  new MidiPotentiometer(A0, MIDI_CTRL_ROW_THREE, MIDI_CHANNEL_FOUR),
  new MidiPotentiometer(A0, MIDI_CTRL_ROW_FOUR,  MIDI_CHANNEL_FOUR)
};

void setup() { 
  Serial.begin(115200);
  multiplexer->setup();
}
// Waits till one byte is ready to read
int readOneByte() {
  while (Serial.available() == 0) {};
  return Serial.read();
}

void sysex_cleanup() {
  while (Serial.available() != 0) {
    int byte = Serial.read();
    if (byte == MIDI_SYSEX_END) break;
  }
}

void sysex_send_identity_reply(int sysex_channel) {
  DBG("MIDI IN: SYSEX: Sending identity");
  Serial.write(MIDI_SYSEX);
  Serial.write(MIDI_SYSEX_TYPE_NON_REALTIME);
  Serial.write(sysex_channel);
  Serial.write(MIDI_SYSEX_GENERAL_INFORMATION);
  Serial.write(MIDI_SYSEX_REPLY_IDENTITY);

  Serial.write(IDENTITY_MANUFACTURER_ID);
  Serial.write(IDENTITY_FAMILY_CODE_ONE);
  Serial.write(IDENTITY_FAMILY_CODE_TWO);
  Serial.write(IDENTITY_MODEL_NUMBER_ONE);
  Serial.write(IDENTITY_MODEL_NUMBER_TWO);
  Serial.write(IDENTITY_VERSION_NUMBER_ONE);
  Serial.write(IDENTITY_VERSION_NUMBER_TWO);
  Serial.write(IDENTITY_VERSION_NUMBER_THREE);
  Serial.write(IDENTITY_VERSION_NUMBER_FOUR);

  Serial.write(MIDI_SYSEX_END);
}

void process_midi_read() {
  if (Serial.available() == 0) return;
  int first_byte = Serial.read();

  // Handle sysex
  if (first_byte == MIDI_SYSEX) {
    // Implement sysex identity request
    // http://midi.teragonaudio.com/tech/midispec/identity.htm
    DBG("MIDI IN: SYSEX");
    int sysex_type = Serial.read();
    int sysex_channel = Serial.read();
    int sub_id = Serial.read();
    if (sub_id != MIDI_SYSEX_GENERAL_INFORMATION) {
      DBG("MIDI IN: SYSEX: ERROR: Unknown sub_id");
      sysex_cleanup();
      return;
    }
    int sub_id2 = readOneByte();
    if (sub_id2 != MIDI_SYSEX_REQUEST_IDENTITY) {
      DBG("MIDI IN: SYSEX: ERROR: Unknown sub_id2");
      sysex_cleanup();
      return;
    }
    if (readOneByte() != 0xF7) {
      DBG("MIDI IN: SYSEX: ERROR: Request did not end with MIDI_SYSEX_END");
      sysex_cleanup();
      return;
    }
    // We arrived at a successful midi sysex request for identity
    sysex_send_identity_reply(sysex_channel);
    
    return;
  }

}

void loop() {
  process_midi_read();
  for (int i = 0; i<16; i++) {
    multiplexer->select(i);
    auto component = components[i];
    component->process();
  }
}
