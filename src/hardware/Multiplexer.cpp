#include "Multiplexer.h"

byte MULTIPLEXER_CONTROL_PINS[] = {
  B00000000, 
  B00000100,
  B00001000,
  B00001100,
  B00010000,
  B00010100,
  B00011000,
  B00011100,
  B00100000,
  B00100100,
  B00101000,
  B00101100,
  B00110000,
  B00110100,
  B00111000,
  B00111100
};

Multiplexer::Multiplexer() {}

void Multiplexer::setup() {
  DDRD  = B00111100; // Set digital pins 2 - 6 as output
  PORTD = B00111100; // Set digital pins 2 - 6 HIGH
}

void Multiplexer::process() { }

void Multiplexer::select(unsigned int select) {
  if (select >= 16) {
    return;
  }
  PORTD = MULTIPLEXER_CONTROL_PINS[select];
}
