

# 4x4 - Versatile MIDI controller built from trash 

## Description

A versatile MIDI controller featuring 16 Potentiometers to map Effects, host innovative EQs or to get creative in mapping it to whatever Parameters you want.

## Pictures

<img src="https://codeberg.org/obsoleszenz/dj-controller-4x4/raw/commit/828026ed6b3f3a7fb0f79e1081a9a949822af39e/docs/top.jpg" alt="Picture of the finished controller" height="300px"/>

<img src="https://codeberg.org/obsoleszenz/dj-controller-4x4/raw/commit/828026ed6b3f3a7fb0f79e1081a9a949822af39e/docs/top-half-open-2.jpg" alt="Picture of the finished controller and open case" height="300px"/>

<img src="https://codeberg.org/obsoleszenz/dj-controller-4x4/raw/branch/main/docs/assembled-perf-boards.jpg" alt="Picture of the perfboard and arduino" height="300px"/>
